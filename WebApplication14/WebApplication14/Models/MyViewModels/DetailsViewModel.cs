﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebApplication14.Models.MyViewModels
{
    public class DetailsViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Foto { get; set; }
        public double Rating { get; set; }
        public string NewRating { get; set; }
        public Comment Comment { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        public IFormFile newFoto { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public int Id { get; set; }
    }
}
