﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebApplication14.Models.MyViewModels
{
    public class CreateCompanyViewModel
    {
        [Required(ErrorMessage = "Укажите название")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Введите описание")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Добавьте фотографию")]
        public IFormFile Foto { get; set; }
    }
}
