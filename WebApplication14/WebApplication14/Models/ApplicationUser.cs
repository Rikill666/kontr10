﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace WebApplication14.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public IEnumerable<Company> Companies { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
    }
}
