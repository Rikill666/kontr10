﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PagedList;
using WebApplication14.Data;
using WebApplication14.Models;

namespace WebApplication14.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private const int pageSize = 20;
        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index(string search_word, int page = 1)
        {
            ViewBag.Search_word = search_word;
            var companies = _context.Company.Where(u => u.Name != null);

            if (!string.IsNullOrWhiteSpace(search_word))
            {
                companies = companies.Where(u => u.Name.Contains(search_word) || u.Description.Contains(search_word));
            }


            IPagedList<Company> companiesForPages = companies.ToPagedList(page, pageSize);
            return View(companiesForPages);
 
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
