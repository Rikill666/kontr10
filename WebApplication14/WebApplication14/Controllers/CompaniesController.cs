﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication14.Data;
using WebApplication14.Models;
using WebApplication14.Models.MyViewModels;
using WebApplication14.Services;

namespace WebApplication14.Controllers
{
    public class CompaniesController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;
        private readonly UploadFile _fileUploadService;

        public CompaniesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager,
            IHostingEnvironment environment, UploadFile fileUploadService)
        {
            _context = context;
            _userManager = userManager;
            _environment = environment;
            _fileUploadService = fileUploadService;
        }



        // GET: Companies/Details/5
        public async Task<IActionResult> Detail(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            var company = await _context.Company.FirstOrDefaultAsync(m => m.Id == id);
            var comments = await _context.Comment.Where(c => c.CompanyId == company.Id).Include(c=>c.User).OrderByDescending(c=>c.DateCreate).ToListAsync();
            var images = await _context.Image.Where(c => c.CompanyId == company.Id).ToListAsync();
            if (user != null)
            {
                ViewBag.UserId = user.Id;
            }
            
            DetailsViewModel model = new DetailsViewModel
            {
                Id = company.Id,
                Comments = comments,
                Images = images,
                Name = company.Name,
                Description = company.Description,
                Foto = company.Foto,
                Rating = company.Rating
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Detail(DetailsViewModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            var company = await _context.Company.FirstOrDefaultAsync(m => m.Id == model.Id);
            if (model.Comment != null)
            {
                Comment comment = new Comment
                {
                    Rating = Convert.ToInt32(model.NewRating),
                    Description = model.Comment.Description,
                    DateCreate = DateTime.Now,
                    UserId = user.Id,
                    CompanyId = model.Id
                };
                
                company.CountOfVotes += 1;
                var rate = _context.Comment.Where(s=>s.CompanyId == company.Id).Select(s => s.Rating);
                company.Rating = (rate.Sum() + Convert.ToDouble(model.NewRating)) / company.CountOfVotes;
                _context.Add(comment);
            }

            if (model.newFoto != null)
            {
                var path = Path.Combine(_environment.WebRootPath, $"image\\{user.UserName}\\post");
                _fileUploadService.Upload(path, model.newFoto.FileName, model.newFoto);
                Image image = new Image
                {
                    UserId = user.Id,
                    CompanyId = company.Id,
                    Foto = $"image/{user.UserName}/post/{model.newFoto.FileName}"
                };
                _context.Add(image);
                company.CountFotos += 1;
            }   
            _context.Update(company);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateCompanyViewModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            if (ModelState.IsValid)
            {
                Company company = new Company
                {
                    UserId = user.Id,
                    Name = model.Name,
                    Description = model.Description,
                    DateCreate = DateTime.Now,
                    Rating = 0 ,
                    CountOfVotes = 0
                };
                _context.Add(company);
                await _context.SaveChangesAsync();
                var path = Path.Combine(_environment.WebRootPath, $"image\\{company.Name}\\post");
                _fileUploadService.Upload(path, model.Foto.FileName, model.Foto);
                company.Foto = $"image/{company.Name}/post/{model.Foto.FileName}";
                _context.Update(company);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index","Home");
            }

            return View(model);
        }

        // GET: Companies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company.SingleOrDefaultAsync(m => m.Id == id);
            if (company == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", company.UserId);
            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Foto,DateCreate,UserId,Rating")] Company company)
        {
            if (id != company.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(company);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyExists(company.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", "Home");
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", company.UserId);
            return View(company);
        }

        // GET: Companies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company
                .Include(c => c.User)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var company = await _context.Company.SingleOrDefaultAsync(m => m.Id == id);
            _context.Company.Remove(company);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        private bool CompanyExists(int id)
        {
            return _context.Company.Any(e => e.Id == id);
        }
    }
}
