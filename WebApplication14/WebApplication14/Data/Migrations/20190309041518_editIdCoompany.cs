﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebApplication14.Data.Migrations
{
    public partial class editIdCoompany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_Company_CompanyId1",
                table: "Comment");

            migrationBuilder.DropForeignKey(
                name: "FK_Image_Company_CompanyId1",
                table: "Image");

            migrationBuilder.DropIndex(
                name: "IX_Image_CompanyId1",
                table: "Image");

            migrationBuilder.DropIndex(
                name: "IX_Comment_CompanyId1",
                table: "Comment");

            migrationBuilder.DropColumn(
                name: "CompanyId1",
                table: "Image");

            migrationBuilder.DropColumn(
                name: "CompanyId1",
                table: "Comment");

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                table: "Image",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                table: "Comment",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Image_CompanyId",
                table: "Image",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_CompanyId",
                table: "Comment",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_Company_CompanyId",
                table: "Comment",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Image_Company_CompanyId",
                table: "Image",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_Company_CompanyId",
                table: "Comment");

            migrationBuilder.DropForeignKey(
                name: "FK_Image_Company_CompanyId",
                table: "Image");

            migrationBuilder.DropIndex(
                name: "IX_Image_CompanyId",
                table: "Image");

            migrationBuilder.DropIndex(
                name: "IX_Comment_CompanyId",
                table: "Comment");

            migrationBuilder.AlterColumn<string>(
                name: "CompanyId",
                table: "Image",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CompanyId1",
                table: "Image",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyId",
                table: "Comment",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CompanyId1",
                table: "Comment",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Image_CompanyId1",
                table: "Image",
                column: "CompanyId1");

            migrationBuilder.CreateIndex(
                name: "IX_Comment_CompanyId1",
                table: "Comment",
                column: "CompanyId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_Company_CompanyId1",
                table: "Comment",
                column: "CompanyId1",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Image_Company_CompanyId1",
                table: "Image",
                column: "CompanyId1",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
